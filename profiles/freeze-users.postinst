#!/bin/sh

num_users=3
freeze_template_user="freezetemplate"
freeze_template_fullname="Modelo para Freeze"

addgroup freeze
adduser --disabled-login --gecos "${freeze_template_fullname}" --shell /bin/bash ${freeze_template_user}
echo "${freeze_template_user}:freeze" | chpasswd

for i in $(seq ${num_users})
do
    username="aluno$(( i - 1 ))"
    fullname="Aluno #$(( i - 1 ))"
    adduser --disabled-login --gecos "${fullname}" --shell /bin/bash ${username}
    adduser ${username} freeze
    echo "${username}:${username}" | chpasswd
done

su ${freeze_template_user} -c "env LANG=pt_BR.UTF-8 xdg-user-dirs-update"
su ${freeze_template_user} -c "mkdir ~/.config/autostart"
su ${freeze_template_user} -c "echo -e '[Desktop Entry]\nType=Application\nName=Autostart commands for freeze sessions\nExec=oi-debian-freeze-session-auto' > ~/.config/autostart/oi-debian-freeze-session-auto.desktop"

for i in blueman light-locker print-applet update-notifier xfce4-power-manager
do
    su ${freeze_template_user} -c "echo -e '[Desktop Entry]\nHidden=true' > ~/.config/autostart/${i}.desktop"
done

# When a frozen user terminates its session, some processes
# may be left behind, preventing its temporary directories from
# being unmounted properly by pam_mount, which can frustrate the
# freeze magic until next reboot.
#
# In order to avoid this, we'll force systemd-logind to kill all
# user processes on logout, so pam_mount can unmount all user
# temporary directories properly.
sed -i -e 's/.*KillUserProcesses=.*/KillUserProcesses=yes/' /etc/systemd/logind.conf
