oi-debian-default-settings (0.5.9) unstable; urgency=medium

  * Bring xfce4-panel back to bottom of screen.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 22 Sep 2017 13:15:34 -0300

oi-debian-default-settings (0.5.8) unstable; urgency=medium

  * Initalize font substitution table for LibreOffice:
    - Calibri -> Carlito
    - Cambria -> Caladea

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 22 Sep 2017 12:31:33 -0300

oi-debian-default-settings (0.5.7) unstable; urgency=medium

  * Disable titlebar hiding for maximized windows after
    reverting panel layout.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 22 Sep 2017 11:03:12 -0300

oi-debian-default-settings (0.5.6) unstable; urgency=medium

  * Revert panel layout to that one in v0.5.2 (more Windows-like)

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 22 Sep 2017 10:29:07 -0300

oi-debian-default-settings (0.5.5) unstable; urgency=medium

  * Fix generic username occurrences in /etc/skel/.config/Kingsoft/Office.conf

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 20 Sep 2017 09:32:10 -0300

oi-debian-default-settings (0.5.4) unstable; urgency=medium

  * Add /etc/skel settings for WPS Office.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 20 Sep 2017 07:53:02 -0300

oi-debian-default-settings (0.5.3) unstable; urgency=medium

  * New Xfce settings to optimize screen height usage:
    - Fix panel on top (like Xubuntu default setting).
    - Hide title bar for maximized windows bt default.
    - Add xfce4-windowck-plugin to show window buttons
      on top panel for maximized windows.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 19 Sep 2017 14:46:09 -0300

oi-debian-default-settings (0.5.2) unstable; urgency=medium

  * Hide borders in xfce4-panel systray applet.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 18 Sep 2017 15:22:43 -0300

oi-debian-default-settings (0.5.1) unstable; urgency=medium

  * Change Xfce panel default height back to 24.
  * Replace some Firefox ESR shortcuts with generic exo-web-browser ones.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 18 Sep 2017 08:45:30 -0300

oi-debian-default-settings (0.5.0) unstable; urgency=medium

  * Drop *-lxde package.
  * Merge *-common and *-xfce4 packages into a single oi-debian-default-settings one.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 18 Sep 2017 08:11:19 -0300

oi-debian-default-settings (0.4.6) unstable; urgency=medium

  * Temporary fix for LXPanel settings installation directory.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 15:19:07 -0300

oi-debian-default-settings (0.4.5) unstable; urgency=medium

  * Fix context menu actions for LXDE.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 14:27:09 -0300

oi-debian-default-settings (0.4.4) unstable; urgency=medium

  * Fix Openbox settings installation directory.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 14:18:44 -0300

oi-debian-default-settings (0.4.3) unstable; urgency=medium

  * Fix LXSession setings installation directory.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 13:51:56 -0300

oi-debian-default-settings (0.4.2) unstable; urgency=medium

  * Another fix for preinst/postrm scripts.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 13:20:59 -0300

oi-debian-default-settings (0.4.1) unstable; urgency=medium

  * Fix preinst/postrm script for package *-lxde.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 13:14:38 -0300

oi-debian-default-settings (0.4.0) unstable; urgency=medium

  * Replace LXQt with LXDE in default settings:
    - Drop package *-lxqt
    - Add new package *-lxde
  * Replace Breeze with Greybird as default theme once again.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 12:19:57 -0300

oi-debian-default-settings (0.3.2) unstable; urgency=medium

  * Fix Breeze icon theme name in LXQt/Xfce settings.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 15 Sep 2017 08:05:31 -0300

oi-debian-default-settings (0.3.1) unstable; urgency=medium

  * Add LXQt theme frost-light to package *-lxqt.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 14 Sep 2017 17:20:21 -0300

oi-debian-default-settings (0.3.0) unstable; urgency=medium

  * Reverted default Qt/GTK/icon/cursor theme to Breeze

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 14 Sep 2017 17:12:56 -0300

oi-debian-default-settings (0.2.8) unstable; urgency=medium

  * Small changes in xfce4-desktop default settings:
    - Disable all icons from desktop

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 13 Sep 2017 12:24:09 -0300

oi-debian-default-settings (0.2.7) unstable; urgency=medium

  * Small changes in xfce4-panel default settings:
    - Set default height to 30px
    - Swap volume control and system tray applets

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 13 Sep 2017 10:38:13 -0300

oi-debian-default-settings (0.2.6) unstable; urgency=medium

  * Add lightdm setting to prevent Xorg screen blanking on idle

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 13 Sep 2017 09:39:58 -0300

oi-debian-default-settings (0.2.5) unstable; urgency=medium

  * Change cursor theme setting to system default in both
    LXQt and Xfce desktop environments

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 12 Sep 2017 12:19:50 +0000

oi-debian-default-settings (0.2.4) unstable; urgency=medium

  * Fix xfwm4 settings to match Numix theme
  * Change default panel position to bottom left corner

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 12 Sep 2017 11:13:39 +0000

oi-debian-default-settings (0.2.3) unstable; urgency=medium

  * Small changes in org.lxqt.juffed.policy file

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 11 Sep 2017 20:08:50 +0000

oi-debian-default-settings (0.2.2) unstable; urgency=medium

  * Install openbox default settings to /etc/skel/.config/openbox folder
  * Reintroduce PolicyKit settings for LXQt core apps

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 11 Sep 2017 19:42:40 +0000

oi-debian-default-settings (0.2.1) unstable; urgency=medium

  * Add missing openbox settings to *-lxqt package

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 11 Sep 2017 18:56:49 +0000

oi-debian-default-settings (0.2.0) unstable; urgency=medium

  * Moved files /etc/xdg/oi-debian/gtk-* directories
    from *-common to *-lxqt package

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 11 Sep 2017 17:59:29 +0000

oi-debian-default-settings (0.1.4) unstable; urgency=medium

  * Replaced Greybird/elementary-xfce with Numix/Numix-Circle
    as default GTK/icon theme
  * Added Numix-Square as an optional icon theme
  * Reintroduced openbox as default window manager for LXQt

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 11 Sep 2017 16:49:44 +0000

oi-debian-default-settings (0.1.3) unstable; urgency=medium

  * Change some xfce4-panel settings
  * Change some compton settings

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 06 Sep 2017 19:57:02 +0000

oi-debian-default-settings (0.1.2) unstable; urgency=medium

  * Fix hinting configuration for LightDM GTK Greeter and GTK applications

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 06 Sep 2017 18:47:35 +0000

oi-debian-default-settings (0.1.1) unstable; urgency=medium

  * Fix DMZ-White cursor theme name in settings files
  * Fix gtk2 theme name for LXQt
  * Replace pkexec with lxqt-sudo for LXQt apps

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 06 Sep 2017 17:53:18 +0000

oi-debian-default-settings (0.1.0) unstable; urgency=medium

  * Merge packages *-lightdm and *-libreoffice into *-common

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 06 Sep 2017 16:18:32 +0000

oi-debian-default-settings (0.0.25) unstable; urgency=medium

  * Set Greybird as default theme for LightDM GTK Greeter

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 06 Sep 2017 16:12:55 +0000

oi-debian-default-settings (0.0.24) unstable; urgency=medium

  * Replace Breeze with Greybird as default theme for both Xfce and LXQt
  * Import a lot of settings from Xubuntu

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 06 Sep 2017 11:50:35 +0000

oi-debian-default-settings (0.0.23) unstable; urgency=medium

  * Add PolicyKit file to allow runing Mousepad as root (Xfce only)

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 05 Sep 2017 13:54:11 +0000

oi-debian-default-settings (0.0.22) unstable; urgency=medium

  * Add new context menu entries for Xfce

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 05 Sep 2017 11:39:19 +0000

oi-debian-default-settings (0.0.21) unstable; urgency=medium

  * Fix file list for package oi-debian-default-settings-xfce4

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 04 Sep 2017 19:04:33 +0000

oi-debian-default-settings (0.0.20) unstable; urgency=medium

  * Add PolicyKit settings for running terminal/file manager/text editor
    as root

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 31 Aug 2017 16:43:47 +0000

oi-debian-default-settings (0.0.19) unstable; urgency=medium

  * Improve /etc/fonts/conf.d/99-chromium.conf settings

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 11 Aug 2017 15:41:39 -0300

oi-debian-default-settings (0.0.18) unstable; urgency=medium

  * Remove chromium launcher from LXQt panel default settings
  * Add /etc/fonts/conf.d/99-chromium.conf for fixing ugly
    font rendering under Chromium.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Fri, 11 Aug 2017 15:26:16 -0300

oi-debian-default-settings (0.0.17) unstable; urgency=medium

  * Add default qt5ct.conf file for Xfce

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 17:01:58 -0300

oi-debian-default-settings (0.0.16) unstable; urgency=medium

  * Small fix after changing default terminal command to "qterminal" for PCManFM-Qt
  * Use qt5ct for styling Qt5 apps under Xfce

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 15:50:46 -0300

oi-debian-default-settings (0.0.15) unstable; urgency=medium

  * Change wallpaper mode to "zoom" for LXQt
  * Change default terminal command to "qterminal" for PCManFM-Qt
  * Provide a complementary xfwm4 config under /etc/skel (needed for LXQt only)
  * Enable xdg-user-dirs-gtk-update autostart for LXQt, too

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 14:58:30 -0300

oi-debian-default-settings (0.0.14) unstable; urgency=medium

  * Fix default wallpaper config for LXQt

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 13:13:08 -0300

oi-debian-default-settings (0.0.13) unstable; urgency=medium

  * Added autostart file for running xdg-user-dirs-gtk-update
    under MATE/Xfce desktop environments.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 12:29:31 -0300

oi-debian-default-settings (0.0.12) unstable; urgency=medium

  * Fix typo in xfce4-panel default settings XML file

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 11:29:43 -0300

oi-debian-default-settings (0.0.11) unstable; urgency=medium

  * Reverted xfce4-whiskermenu icon to its default value
  * Diable some icons in xfce4-desktop settings
  * Add/Change some items in xfce4-panel settings

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 09:32:40 -0300

oi-debian-default-settings (0.0.10) unstable; urgency=medium

  * Change location of xfce4-panel default settings file
  * Include default settings for xfce4-whiskermenu-plugin

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Thu, 10 Aug 2017 08:44:08 -0300

oi-debian-default-settings (0.0.9) unstable; urgency=medium

  * New package oi-debian-default-settings-xfce4
  * Added xfwm4 default settings to *-common

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 09 Aug 2017 16:53:21 -0300

oi-debian-default-settings (0.0.8) unstable; urgency=medium

  * Set default Openbox theme to Breeze-ob
  * Set new default font size in Openbox title bar to 12

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 09 Aug 2017 10:17:59 -0300

oi-debian-default-settings (0.0.7) unstable; urgency=medium

  * Set default font style to "Regular" for Qt4 apps

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Wed, 09 Aug 2017 06:59:57 -0300

oi-debian-default-settings (0.0.6) unstable; urgency=medium

  * Update Breeze color pallete for Qt4 apps

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 08 Aug 2017 15:54:17 -0300

oi-debian-default-settings (0.0.5) unstable; urgency=medium

  * Fix GTK3 global settings
  * Add default settings for LibreOffice & Qt4 apps

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 08 Aug 2017 15:05:42 -0300

oi-debian-default-settings (0.0.4) unstable; urgency=medium

  * Install openbox default settings to /etc/skel/.config/openbox/lxqt-rc.xml
  * Fix mistyping in src/etc/X11/Xsession.d/61oi-debian

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 08 Aug 2017 14:08:34 -0300

oi-debian-default-settings (0.0.3) unstable; urgency=medium

  * New src/ folder structure
  * Installation destination changed to /etc/xdg/oi-debian
  * Xsession script renamed to /etc/X11/Xsession.d/61oi-debian
  * New package oi-debian-default-settings-common
  * Default icon theme changed to Papirus

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 08 Aug 2017 12:34:16 -0300

oi-debian-default-settings (0.0.2) unstable; urgency=medium

  * Added a Xsession script for prepending our folder to XDG_CONFIG_DIRS.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Tue, 08 Aug 2017 09:59:39 -0300

oi-debian-default-settings (0.0.1) unstable; urgency=medium

  * Initial Release.

 -- Laércio de Sousa <laerciosousa@sme-mogidascruzes.sp.gov.br>  Mon, 07 Aug 2017 16:44:19 -0300
