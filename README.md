# OI-Debian

## Requisitos para construir a imagem ISO

* SO de 64 bits
* [VirtualBox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/) + plug-in [vagrant-rsync-back](https://github.com/smerrill/vagrant-rsync-back)
* Pacotes DEB externos não incluídos neste repositório Git (devem ser baixados manualmente e copiados para a pasta `local-packages`):
  * [Omnitux](http://omnitux.sourceforge.net/)

## Instruções

### Instalando o plug-in para o Vagrant
```
vagrant plugin install vagrant-rsync-back
```

### Inicializando e acessando a VM a partir do Vagrant
```
vagrant up
vagrant ssh
```

### Construindo a imagem ISO dentro da VM
(IMPORTANTE: os comandos abaixo devem ser executados **dentro da VM**, ou seja, após o comando `vagrant ssh`)
```
cd /vagrant
./build.sh
exit (para voltar para o SO hospedeiro)
```

### Copiando a imagem ISO recém-construída de volta para o SO hospedeiro
```
vagrant rsync-back
```

Feito isto, você pode testar a imagem ISO normalmente em uma nova VM no VirtualBox (não utilize a mesma VM criada pelo Vagrant!)